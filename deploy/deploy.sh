aws s3 rm s3://surveygamez.com/ --recursive
aws s3 cp . s3://surveygamez.com --recursive --exclude "*.git*" --exclude "deploy*"
